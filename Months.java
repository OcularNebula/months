import javax.swing.*;
public class Months{
	public enum Month {January, February, March, April, May, June, July, August, September, October, November, December}
	public static void main(String[] args){

	Month[] choices = {Month.January, Month.February, Month.March, Month.April, Month.May, Month.June, Month.July, Month.August, Month.September, Month.October, Month.November, Month.December};
	Month input = (Month) JOptionPane.showInputDialog(null, "What month is it?", "Month", JOptionPane.INFORMATION_MESSAGE, null, choices, choices[11]);
	while (input != null){
		switch(input){
		case December: case January: case February:
			JOptionPane.showMessageDialog(null, "Please build a snowman immediately.");
			break;
		case March: case April: case May:
			JOptionPane.showMessageDialog(null, "Spring is happier than other seasons.");
			break;
		case June: case July: case August:
			JOptionPane.showMessageDialog(null, "I have nothing to say about summer.");
			break;
		case September: case October: case November:
			JOptionPane.showMessageDialog(null, "There is foliage all around us.");
			break;
		default:
			JOptionPane.showMessageDialog(null, "Please enter a month that exists normatively.");
			break;
			}
		input = (Month) JOptionPane.showInputDialog(null, "What month is it?", "Month", JOptionPane.INFORMATION_MESSAGE, null, choices, choices[11]);
		}
	}
}
